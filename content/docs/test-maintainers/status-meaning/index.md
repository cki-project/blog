---
title: Reporting KCIDB results
description: >
  What KCIDB status values mean in the context of CKI, and how they can be
  influenced from Beaker/Restraint
weight: 30
---

## Rationale

The following figure shows the available KCIDB status values and the areas of
concern for the non-successful values.

![Result-status mapping](mapping.png)

Where possible, Beaker/Restraint-based test code should be able to select the
most appropriate KCIDB status value via the CLI tools provided by Restraint and
the resulting Beaker XML file.

## Selecting KCIDB status codes from a Restraint/Beaker task

| Result  | Code                                             |
|---------|--------------------------------------------------|
| `SKIP`  | `rstrnt-report-result / SKIP`                    |
| `DONE`  | not available                                    |
| `PASS`  | `rstrnt-report-result / PASS` or `exit 0`        |
| `MISS`  | `rstrnt-abort` to skip subsequent tests          |
| `ERROR` | `rstrnt-report-result / WARN`                    |
| `FAIL`  | `rstrnt-report-result / FAIL`, `exit 1` or Panic |

More details and test cases can be found in the [Background][kcidb] section.

[kcidb]: ../../background/kernel-testing/status-meaning/index.md
