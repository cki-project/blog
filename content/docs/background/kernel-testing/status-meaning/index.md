---
title: Restraint KCIDB reporting
description: >
  Test cases for Beaker/Restraint result reporting and the resulting XML files
weight: 20
---

## SKIP

The `SKIP` KCIDB status will be used for Restraint tasks when all results
reported as skipped.

```bash
rstrnt-report-result / SKIP
```

Restraint:

```xml
<task status="Completed" result="None" start_time>
  <results><result result="SKIP" /></result>
</task>
```

Beaker:

```xml
<task status="Completed" result="Skip" start_time />
```

### Details for SKIP

Click on any of the arrows to view the corresponding file:
{{< details "runtest.sh" >}}
{{< readfile file="restraint/skip/runtest.sh" code="true" lang="bash" >}}
{{< /details >}}
{{< details "metadata" >}}
{{< readfile file="restraint/skip/metadata" code="true" lang="ini" >}}
{{< /details >}}
{{< details "job.xml" >}}
{{< readfile file="restraint/skip/job.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "restraint.xml" >}}
{{< readfile file="restraint/skip/results/restraint.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "beaker.xml" >}}
{{< readfile file="restraint/skip/results/beaker.xml" code="true" lang="xml" >}}
{{< /details >}}

## DONE

The `DONE` KCIDB status is not available via Restraint-based tests.

## PASS

### Only exit code 0

Test code:

```bash
exit 0
```

Restraint output:

```xml
<task status="Completed" result="PASS" start_time>
  <results>
    <result path="exit_code" result="PASS" />
  </results>
</task>
```

Beaker output:

```xml
<task status="Completed" result="Pass" start_time>
  <results>
    <result path="exit_code" result="Pass" />
  </results>
</task>
```

### PASS test result reported

Test code:

```bash
rstrnt-report-result / PASS
```

Restraint output:

```xml
<task status="Completed" result="PASS" start_time>
  <results>
    <result result="PASS" />
  </results>
</task>
```

Beaker output:

```xml
<task status="Completed" result="Pass" start_time>
  <results>
    <result result="Pass" />
  </results>
</task>
```

### Details for PASS

Only exit code 0:
{{< details "runtest.sh" >}}
{{< readfile file="restraint/exit0/runtest.sh" code="true" lang="bash" >}}
{{< /details >}}
{{< details "metadata" >}}
{{< readfile file="restraint/exit0/metadata" code="true" lang="ini" >}}
{{< /details >}}
{{< details "job.xml" >}}
{{< readfile file="restraint/exit0/job.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "restraint.xml" >}}
{{< readfile file="restraint/exit0/results/restraint.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "beaker.xml" >}}
{{< readfile file="restraint/exit0/results/beaker.xml" code="true" lang="xml" >}}
{{< /details >}}

PASS test result reported:
{{< details "runtest.sh" >}}
{{< readfile file="restraint/pass/runtest.sh" code="true" lang="bash" >}}
{{< /details >}}
{{< details "metadata" >}}
{{< readfile file="restraint/pass/metadata" code="true" lang="ini" >}}
{{< /details >}}
{{< details "job.xml" >}}
{{< readfile file="restraint/pass/job.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "restraint.xml" >}}
{{< readfile file="restraint/pass/results/restraint.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "beaker.xml" >}}
{{< readfile file="restraint/pass/results/beaker.xml" code="true" lang="xml" >}}
{{< /details >}}

## MISS

The `MISS` KCIDB status is used for tasks that would have run after an
aborted tasks.

When aborting tasks, the result of the aborted tasks will differ between
Restraint and Beaker. While Restraint will happily use any result
(None/PASS/WARN/FAIL), Beaker will "upgrade" the task result to at least WARN.

### No result set before aborting

Test code (previous task):

```bash
rstrnt-abort
```

Restraint output:

```xml
<task status="Aborted" result="None" start_time />
<task status="Aborted" result="None" />
```

Beaker output:

```xml
<task status="Aborted" result="Warn" start_time>
  <results>
    <result result="Warn" />
  </results>
</task>
<task status="Aborted" result="Warn">
  <results>
    <result result="Warn" />
  </results>
</task>
```

### PASS test result reported before aborting

Test code:

```bash
rstrnt-report-result / PASS
rstrnt-abort
```

Restraint output:

```xml
<task status="Aborted" result="PASS" start_time>
  <results>
    <result result="Pass" />
  </results>
</task>
<task status="Aborted" result="None" />
```

Beaker output:

```xml
<task status="Aborted" result="Warn" start_time>
  <results>
    <result result="Pass" />
    <result result="Warn" />
  </results>
</task>
<task status="Aborted" result="Warn">
  <results>
    <result result="Warn" />
  </results>
</task>
```

### SKIP test result reported before aborting

Test code:

```bash
rstrnt-report-result / SKIP
rstrnt-abort
```

Restraint output:

```xml
<task status="Aborted" result="None" start_time>
  <results>
    <result result="SKIP" />
  </results>
</task>
<task status="Aborted" result="None" />
```

Beaker output:

```xml
<task status="Aborted" result="Warn" start_time>
  <results>
    <result result="Skip" />
    <result result="Warn" />
  </results>
</task>
<task status="Aborted" result="Warn">
  <results>
    <result result="Warn" />
  </results>
</task>
```

### WARN test result reported before aborting

Test code:

```bash
rstrnt-report-result / WARN
rstrnt-abort
```

Restraint output:

```xml
<task status="Aborted" result="WARN" start_time>
  <results>
    <result result="WARN" />
  </results>
</task>
<task status="Aborted" result="None" />
```

Beaker output:

```xml
<task status="Aborted" result="Warn" start_time>
  <results>
    <result result="Warn" />
    <result result="Warn" />
  </results>
</task>
<task status="Aborted" result="Warn">
  <results>
    <result result="Warn" />
  </results>
</task>
```

### FAIL test result reported before aborting

Test code:

```bash
rstrnt-report-result / FAIL
rstrnt-abort
```

Restraint output:

```xml
<task status="Aborted" result="FAIL" start_time>
  <results>
    <result result="FAIL" />
  </results>
</task>
<task status="Aborted" result="None" />
```

Beaker output:

```xml
<task status="Aborted" result="Fail" start_time>
  <results>
    <result result="Fail" />
    <result result="Warn" />
  </results>
</task>
<task status="Aborted" result="Warn">
  <results>
    <result result="Warn" />
  </results>
</task>
```

### Details for MISS

No result set before aborting:
{{< details "runtest.sh" >}}
{{< readfile file="restraint/abort/runtest.sh" code="true" lang="bash" >}}
{{< /details >}}
{{< details "metadata" >}}
{{< readfile file="restraint/abort/metadata" code="true" lang="ini" >}}
{{< /details >}}
{{< details "job.xml" >}}
{{< readfile file="restraint/abort/job.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "restraint.xml" >}}
{{< readfile file="restraint/abort/results/restraint.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "beaker.xml" >}}
{{< readfile file="restraint/abort/results/beaker.xml" code="true" lang="xml" >}}
{{< /details >}}

PASS test result reported before aborting:
{{< details "runtest.sh" >}}
{{< readfile file="restraint/abortpass/runtest.sh" code="true" lang="bash" >}}
{{< /details >}}
{{< details "metadata" >}}
{{< readfile file="restraint/abortpass/metadata" code="true" lang="ini" >}}
{{< /details >}}
{{< details "job.xml" >}}
{{< readfile file="restraint/abortpass/job.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "restraint.xml" >}}
{{< readfile file="restraint/abortpass/results/restraint.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "beaker.xml" >}}
{{< readfile file="restraint/abortpass/results/beaker.xml" code="true" lang="xml" >}}
{{< /details >}}

SKIP test result reported before aborting:
{{< details "runtest.sh" >}}
{{< readfile file="restraint/abortskip/runtest.sh" code="true" lang="bash" >}}
{{< /details >}}
{{< details "metadata" >}}
{{< readfile file="restraint/abortskip/metadata" code="true" lang="ini" >}}
{{< /details >}}
{{< details "job.xml" >}}
{{< readfile file="restraint/abortskip/job.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "restraint.xml" >}}
{{< readfile file="restraint/abortskip/results/restraint.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "beaker.xml" >}}
{{< readfile file="restraint/abortskip/results/beaker.xml" code="true" lang="xml" >}}
{{< /details >}}

WARN test result reported before aborting:
{{< details "runtest.sh" >}}
{{< readfile file="restraint/abortwarn/runtest.sh" code="true" lang="bash" >}}
{{< /details >}}
{{< details "metadata" >}}
{{< readfile file="restraint/abortwarn/metadata" code="true" lang="ini" >}}
{{< /details >}}
{{< details "job.xml" >}}
{{< readfile file="restraint/abortwarn/job.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "restraint.xml" >}}
{{< readfile file="restraint/abortwarn/results/restraint.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "beaker.xml" >}}
{{< readfile file="restraint/abortwarn/results/beaker.xml" code="true" lang="xml" >}}
{{< /details >}}

FAIL test result reported before aborting:
{{< details "runtest.sh" >}}
{{< readfile file="restraint/abortfail/runtest.sh" code="true" lang="bash" >}}
{{< /details >}}
{{< details "metadata" >}}
{{< readfile file="restraint/abortfail/metadata" code="true" lang="ini" >}}
{{< /details >}}
{{< details "job.xml" >}}
{{< readfile file="restraint/abortfail/job.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "restraint.xml" >}}
{{< readfile file="restraint/abortfail/results/restraint.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "beaker.xml" >}}
{{< readfile file="restraint/abortfail/results/beaker.xml" code="true" lang="xml" >}}
{{< /details >}}

## ERROR

Test code:

```bash
rstrnt-report-result / WARN
```

Restraint:

```xml
<task status="Completed" result="WARN" start_time>
  <results>
    <result result="WARN" />
  </results>
</task>
```

Beaker:

```xml
<task status="Completed" result="Warn" start_time>
  <results>
    <result result="Warn" />
  </results>
</task>
```

### Details for ERROR

{{< details "runtest.sh" >}}
{{< readfile file="restraint/warn/runtest.sh" code="true" lang="bash" >}}
{{< /details >}}
{{< details "metadata" >}}
{{< readfile file="restraint/warn/metadata" code="true" lang="ini" >}}
{{< /details >}}
{{< details "job.xml" >}}
{{< readfile file="restraint/warn/job.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "restraint.xml" >}}
{{< readfile file="restraint/warn/results/restraint.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "beaker.xml" >}}
{{< readfile file="restraint/warn/results/beaker.xml" code="true" lang="xml" >}}
{{< /details >}}

## FAIL

### Exiting with a non-zero exit code

Test code:

```bash
exit 1
```

Restraint:

```xml
<task status="Completed" result="FAIL" start_time>
  <results>
    <result path="exit_code" result="FAIL" />
  </results>
</task>
```

Beaker:

```xml
<task status="Completed" result="Fail" start_time />
  <results>
    <result path="exit_code" result="Fail" />
  </results>
</task>
```

### FAIL test result reported

Test code:

```bash
rstrnt-report-result / FAIL
```

Restraint output:

```xml
<task status="Completed" result="FAIL" start_time>
  <results>
    <result result="FAIL" />
  </results>
</task>
```

Beaker output:

```xml
<task status="Completed" result="Fail" start_time>
  <results>
    <result result="Fail" />
  </results>
</task>
```

### FAIL test result reported and non-zero exit code

Test code:

```bash
rstrnt-report-result / FAIL
exit 1
```

Restraint:

```xml
<task status="Completed" result="FAIL" start_time>
  <results>
    <result result="FAIL" />
    <result path="exit_code" result="FAIL" />
  </results>
</task>
```

Beaker:

```xml
<task status="Completed" result="Fail" start_time>
  <results>
    <result result="Fail" />
    <result path="exit_code" result="Fail" />
  </results>
</task>
```

### With a panic

Test code:

```bash
echo 1 > /proc/sys/kernel/sysrq
echo c > /proc/sysrq-trigger
```

Restraint:

```xml
<task status="Aborted" result="None" start_time />
</task>
```

Beaker:

```xml
<task status="Aborted" result="Panic" start_time />
  <results>
    <result result="Panic" />
  </results>
</task>
```

### Details for FAIL

Exiting with a non-zero exit code:
{{< details "runtest.sh" >}}
{{< readfile file="restraint/exit1/runtest.sh" code="true" lang="bash" >}}
{{< /details >}}
{{< details "metadata" >}}
{{< readfile file="restraint/exit1/metadata" code="true" lang="ini" >}}
{{< /details >}}
{{< details "job.xml" >}}
{{< readfile file="restraint/exit1/job.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "restraint.xml" >}}
{{< readfile file="restraint/exit1/results/restraint.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "beaker.xml" >}}
{{< readfile file="restraint/exit1/results/beaker.xml" code="true" lang="xml" >}}
{{< /details >}}

FAIL test result reported:
{{< details "runtest.sh" >}}
{{< readfile file="restraint/fail/runtest.sh" code="true" lang="bash" >}}
{{< /details >}}
{{< details "metadata" >}}
{{< readfile file="restraint/fail/metadata" code="true" lang="ini" >}}
{{< /details >}}
{{< details "job.xml" >}}
{{< readfile file="restraint/fail/job.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "restraint.xml" >}}
{{< readfile file="restraint/fail/results/restraint.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "beaker.xml" >}}
{{< readfile file="restraint/fail/results/beaker.xml" code="true" lang="xml" >}}
{{< /details >}}

FAIL test result reported and non-zero exit code:
{{< details "runtest.sh" >}}
{{< readfile file="restraint/fail1/runtest.sh" code="true" lang="bash" >}}
{{< /details >}}
{{< details "metadata" >}}
{{< readfile file="restraint/fail1/metadata" code="true" lang="ini" >}}
{{< /details >}}
{{< details "job.xml" >}}
{{< readfile file="restraint/fail1/job.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "restraint.xml" >}}
{{< readfile file="restraint/fail1/results/restraint.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "beaker.xml" >}}
{{< readfile file="restraint/fail1/results/beaker.xml" code="true" lang="xml" >}}
{{< /details >}}

With a panic:
{{< details "runtest.sh" >}}
{{< readfile file="restraint/panic/runtest.sh" code="true" lang="bash" >}}
{{< /details >}}
{{< details "metadata" >}}
{{< readfile file="restraint/panic/metadata" code="true" lang="ini" >}}
{{< /details >}}
{{< details "job.xml" >}}
{{< readfile file="restraint/panic/job.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "restraint.xml" >}}
{{< readfile file="restraint/panic/results/restraint.xml" code="true" lang="xml" >}}
{{< /details >}}
{{< details "beaker.xml" >}}
{{< readfile file="restraint/panic/results/beaker.xml" code="true" lang="xml" >}}
{{< /details >}}

## Reproducing the results above

### Running via Restraint on a Beaker machine

```bash
wget -O /etc/yum.repos.d/beaker-harness.repo \
  https://beaker-project.org/yum/beaker-harness-Fedora.repo
dnf install -y restraint-client
restraint --job job.xml --host 1=root@beaker.host
```

### Submitting to Beaker

```bash
dnf install -y beaker-client krb5-workstation
mkdir ~/.beaker_client
echo 'HUB_URL = "https://beaker.instance.url"' \
  > ~/.beaker_client/config
kinit user@DOMAIN
bkr job-submit job.xml
```
