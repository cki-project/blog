---
title: Code coverage
description: How to enable code coverage for kernel builds.
linkTitle: Kernel Code Coverage
weight: 60
---

Code coverage is a measure used to describe the degree to which the source
code of a program is executed when a particular test suite runs. Enabling
this feature increases the size of the build and slows some operations, which
is why this feature is not optimal for production builds. However, it can be
enabled for test runs of CKI builds.

To achieve that goal, we're using [Gcov] and [Lcov].

## Enabling

This feature is only available for [Trusted Contributors] using rpm.

To enable it, just add `.coverage` to the merge request pipeline in
`.gitlab-ci.yaml`, below you can see an example:

```yaml
...
# regular pipelines

merge_request_regular:
  extends: [.internal, .merge_request, .with_notifications,
            .trigger, .coverage]
...
```

```diff
...
@@ -46,7 +46,7 @@ workflow: !reference [.workflow]
 
 merge_request_regular:
   extends: [.internal, .merge_request, .with_notifications, .rhel_common,
-            .trigger]
+            .trigger, .coverage]
 
 realtime_check_regular:
   extends: [.internal, .merge_request, .realtime_check_common,
```

By default our pipeline will get coverage data from some directories. To see
the directories set by default go to [index.yaml] of [kpet-db] under
`variables` ->  `coverage_dirs` -> `default`.

Reminder: changes to `.gitlab-ci.yaml` for enabling code coverage tests
should be removed before marking merge requests ready for review.

## Getting the results

Links for both the coverage report and a tarball file which contains all the
information necessary to generate the HTML report are provided. This will be
linked directly in either the DataWarehouse for each gcov kernel result or it
can be found by searching for the links within the GitLab job console log.

No matter which option is chosen, you will get the coverage report

![Coverage report](coverage_report.png)

and the tarball file which contents should look like (we will not guarantee a
specific layout of the files).

```bash
$ tree
.
├── report
├── info.txt
├── run.done.01
└── run.done.02
└── run.done.0X
```

### GitLab Pipelines

For every architecture, you'll see a GitLab `test` job.

![Test jobs](test_jobs.png)

Click on the test job for each supported arch, go to the bottom and search
for the coverage links.

![Job log](job_log.png)

### Datawarehouse

Every checkout with coverage will be shown with the suffix `.coverage`.

![Datawarehouse_Checkouts](checkout_coverage.png)

Inside of every build you will find the associated coverage links including
the report and tarball file.

![Datawarehouse_Build](build_coverage_links.png)

[Trusted Contributors]: ../mr-full/index.md#trusted-contributors
[Gcov]: https://gcc.gnu.org/onlinedocs/gcc/Gcov.html
[Lcov]: https://github.com/linux-test-project/lcov
[kpet-db]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/
[index.yaml]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/blob/main/index.yaml
