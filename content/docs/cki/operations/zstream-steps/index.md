---
title: Zstreaming a kernel release
description: Steps to do when a kernel stream is to be released.
companion: true
---

## Background

Every once in a while, a RHEL stream is released. As CKI needs to run with
matching environment, we need to update our setup to match this, in an order
that doesn't break the runs in between.

## Timeline and requirements

There is no hard timeline on when these changes need to be put in place. The
only requirement to start the process is to have the kernel zstream branch
created by the maintainers. This happens pretty early, and is announced on
[kernel-info]. Of course, the steps requiring the ystream compose and zstream
containers need to wait for their availability.

A good indicator to start the process is the kernel maintainers changing the
kernel makefile configuration to officially tag the new zstream, or a new
source-git branch is created. This usually happens around the RC release of the
new zstream compose.

Package dependencies and conflicts between the new zstream and ystream may
increase the priority of the changes on CKI side. This can happen e.g. when the
new ystream kernel or the tests start requiring package versions only available
in the new ystream compose. If this is the case, kernel or test maintainers
will point it out, or we'll notice the new pipelines consistently failing.

## Steps to take

1. In [kpet-db], create a new zstream tree. The tree should match the ystream
   tree for now. Also update the [nvr tree mapping configuration] accordingly.
   As the new trees will use nightly composes, the [ystream composes file] also
   needs to be updated. Add a new line for the tree with the same name as defined in
   kpet-db and set the distro_name to the prefix for the beaker compose such as

   ```yaml
   rhel810-z: {distro_name: RHEL-8.10%}
   ```

2. Wait for the kernel maintainer to update the kernel project configuration:
   - in the [public project configuration] for the CentOS Stream kernel
     projects:
      - adjusting the `main-*` branch setup
   - in the internal production and staging configuration for the RHEL kernel
     projects:
      - adjusting the `main-*` branch setup
      - adding a new zstream branch
      - changing the default branch
3. Wait for the kernel maintainer to submit an MR to the kernel repository that

   - adds a new `disttag_override` value to all pipelines
      - a common value of `.elX_Y` where X is the major version and Y is the minor
        version
      - variant pipelines may need a separate override, e.g., `.elX_Yiv` for automotive
   - updates the `kcidb_tree_name` value to refer to the zstream like `rhel-8.0`
   - updates the stream disttag in one of the `redhat/Makefile.*`

   Review the MR to make sure all of the steps above are done correctly, and
   check the MR pipeline picks the new zstream kpet tree and appears under the
   correct KCIDB tree in DataWarehouse.
4. Now that zstream kernels will not use the ystream tree file anymore,
   everything is ready to update the ystream tree file in [kpet-db] to point to
   the new ystream compose.

   In the [ystream composes file], the `bot_comment` section needs to be updated to
   add the call to test the new zstream tree, like:

   ```yaml
   - `[rhel/rhel-9/9.6]`
   ```

5. Once the RHEL zstream is GA, a new UBI container should become available.
   Create a new [builder container image] from it. You should be able to copy
   the definition of an older zstream container image and only override the
   stream value.
6. In [kpet-db], the zstream tree needs to be updated to the released compose. Also
   the [ystream composes file] needs to be updated to remove the tree.
7. Create an MR to the kernel repository for the zstream branch to switch the
   `builder_image` to the newly created container image.
8. Add the new container image to the `.pipeline_images` job template in the
   CKI GitLab CI/CD templates in [cki-lib].

{{% include "internal.md" %}}

[kernel-info]: https://listman.redhat.com/mailman/private/kernel-info/
[public project configuration]: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/utils/rh_metadata.yaml
[kpet-db]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/tree/main/trees
[nvr tree mapping configuration]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/blob/main/nvr-tree-mapping.yml
[ystream composes file]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/blob/main/ystream_composes.yaml
[builder container image]: ../pipeline-images.md
[cki-lib]: https://gitlab.com/cki-project/cki-lib/-/blob/main/.gitlab/ci_templates/cki-templates.yml
