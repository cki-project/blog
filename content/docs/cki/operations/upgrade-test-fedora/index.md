---
title: Changing the Fedora release for the tested Koji builds
linkTitle: Changing Fedora release for Koji tests
description: >
    How to change the Fedora release that has its Koji kernel builds tested
---

According to the official [Fedora Linux Release Life Cycle][lifecycle], Fedora
Linux has releases every six months, with releases being supported for
about 13 months. The [Fedora Project schedules][schedules] contain the details
for the individual releases.

## Steps

1. Update the fedora-latest tree in [kpet-db] to select the correct compose for testing.

   Adjust the `distro_family` and  `distro_name` with the latest Fedora compose
   that has the [RELEASED] tag.

2. Update the rpm_release in [pipeline-data] to select the correct packages for testing.

   Adjust the `rpm_release` field in the `kernel-fedora` section to match the Fedora version
   used in kpet-db.

[lifecycle]: https://docs.fedoraproject.org/en-US/releases/lifecycle/
[schedules]: https://fedorapeople.org/groups/schedule/
[kpet-db]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db
[pipeline-data]: https://gitlab.com/cki-project/pipeline-data
[RELEASED]: https://beaker.engineering.redhat.com/distrotrees/?search-0.table=Tag&search-0.operation=is&search-0.value=RELEASED&search-1.table=OSMajor&search-1.operation=contains&search-1.value=Fedora
